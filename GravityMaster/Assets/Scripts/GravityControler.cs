﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Class responsible for simluating gravity
public class GravityControler : MonoBehaviour {
    private List<GameObject> gravObjectsList;
    private float grav = 1;
    public int l; 

    // Use this for initialization
    void Start () {
        gravObjectsList = new List<GameObject>();

    }

    // On every Fixed Frame each objects is impacted by froce proportional to sum of vectors
    // Forces are symetrical so we can avoid calculating half od cetors
    // If number of graviti objects excedes 250 turns off posibility of creating new graviti objects add revers gravitation force
    void FixedUpdate()
    {
        gravObjectsList.Clear();
        gravObjectsList.AddRange( GameObject.FindGameObjectsWithTag("GravObject"));
        l = gravObjectsList.Count;
        if (l > 250)
        {
            grav = -1;
            GetComponent<Creator>().enabled = false;
            for (int i = 0; i < l; i++)
            {
                gravObjectsList[i].GetComponent<Colisions>().enabled = false;
            }
        }
        Vector3[] forceVectors = new Vector3[l];
        int lengthOfArray = forceVectors.Length;

        for (int i = 0; i < l; i++)
        {
            for (int j = i + 1; j < l; j++)
            {
                Vector3 a = gravObjectsList[i].transform.position;
                Vector3 b = gravObjectsList[j].transform.position;
                Vector3 v = a - b;
                Vector3 diff = v.normalized * gravObjectsList[i].GetComponent<Rigidbody>().mass* gravObjectsList[j].GetComponent<Rigidbody>().mass /(v.magnitude*v.magnitude) ;
                forceVectors[i] += -diff*grav;
                forceVectors[j] += diff*grav;


            }
        }
        for (int i = 0; i < l; i++)
        {
            if (forceVectors[i] != new Vector3())
                gravObjectsList[i].GetComponent<Rigidbody>().AddForce(forceVectors[i],ForceMode.Impulse);
        }
    }
            
}
