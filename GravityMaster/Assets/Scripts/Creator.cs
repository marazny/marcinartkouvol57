﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Class controls all creations off new graviti objects
public class Creator : MonoBehaviour {
    private float time;
    public GameObject planetPrefab;
    public float randomCreationTime;
    
	// Use this for initialization
	void Start () {



    }
	
	// Counts time between creating new random graviti objects
    // Calls CreateRandom if it is time to create new object
	void Update () {
        time += Time.deltaTime;
        if (time > randomCreationTime)
        {
            time -= randomCreationTime;
            CreteRandom();
        }
	}

    //Create new graviti object in random position
    void CreteRandom()
    {
        GameObject planet = Instantiate(planetPrefab, new Vector3(Random.RandomRange(-12, 6), Random.RandomRange(-40, -10), Random.RandomRange(-23, 5)),new Quaternion(0,0,0,0));
    }
    //Controls combining two graviti objects
    // If the mass of new object is too big Calls Explode function
    public void CreatFromColision(GameObject a,GameObject b)
    {
        float aMass = a.GetComponent<Rigidbody>().mass;
        float bMass = b.GetComponent<Rigidbody>().mass;
        a.active = false;
        b.active = false;
        if ((aMass + bMass) > 49)
        {
            Explode(aMass + bMass, (a.transform.position + b.transform.position) / 2);
        }
        else {
            GameObject planet = Instantiate(planetPrefab, (a.transform.position + b.transform.position) / 2, new Quaternion(0, 0, 0, 0));
            planet.GetComponent<Rigidbody>().mass = aMass + bMass;
            planet.GetComponent<MeshRenderer>().material.color = new Color(1-(aMass + bMass)/50, 1 - (aMass + bMass) / 50, 1-(aMass + bMass)/50);
        }
        Destroy(a);
        Destroy(b);
    }

    // Sends amount of new graviti objects equal to combined mass in random directions
    public void Explode(float combinedMass, Vector3 creationPosition)
    {
        for(int i = 0; i < combinedMass; i++)
        {
            GameObject planet = Instantiate(planetPrefab, creationPosition, Quaternion.identity);
            Vector3 force = (new Vector3(Random.value - 0.5f, Random.value - 0.5f, Random.value - 0.5f).normalized )* 100;
            planet.GetComponent<Rigidbody>().AddForce(force,ForceMode.Impulse);
            planet.GetComponent<Colisions>().disableColisions();
        }
    }
    }
