﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Colisions : MonoBehaviour {
    private float time = 0;
    private bool coliderDisabled = false;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update function control switching on coliders 0.5 s after explosion
	void Update () {
        if (coliderDisabled)
        {
            if (time > 0.5)
            {
                switchColiders();
                time = 0;
                coliderDisabled = false;
            }
            else
            {
                time += Time.deltaTime;
            }
        }
	}

    // Colision detector, to avoid detecting colison by two objects only the one with lower hashCode Creats new objets using CreateFromColision method in Creator script
    void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.GetHashCode() > this.gameObject.GetHashCode())
        {
            GameObject.Find("GameControler").GetComponent<Creator>().CreatFromColision(other.gameObject, this.gameObject);
        }
    }

    //Function disables colisions and starts timer to turn colisions on in Update
    public void disableColisions()
    {
        switchColiders();
        coliderDisabled = true;
  
    }

    public void switchColiders()
    {
        Collider[] coliders = GetComponents<Collider>();
        coliders[0].enabled = !coliders[0].enabled;
        coliders[1].enabled = !coliders[1].enabled;

    }
}
