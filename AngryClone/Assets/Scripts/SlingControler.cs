﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Class controls sling
public class SlingControler : MonoBehaviour {

    private LineRenderer slingLine;
    public GameObject projectail;
    public float maxLineLenght;
    public float maxForce;
    public float dist;
    void Start () {
        slingLine = GetComponent<LineRenderer>();
    }
	
	// Update is called once per frame
    // Method might be splited
	void Update () {
        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            
            Vector3 a= slingLine.GetPosition(0)- slingLine.GetPosition(1);
            dist = Vector3.Distance(slingLine.GetPosition(0), slingLine.GetPosition(1));
            projectail.GetComponent<Rigidbody2D>().velocity=(a*(maxForce*(dist/ maxLineLenght)) );
            projectail.transform.GetComponent<Rigidbody2D>().gravityScale = 1;
            GetComponent<Restarter>().clock();
        }
        else
        {
            if (Input.GetKey(KeyCode.Mouse0))
            {
                projectail.transform.GetComponent<SpriteRenderer>().enabled = true;
                projectail.transform.GetComponent<Rigidbody2D>().gravityScale =0;
                slingLine.enabled = true;
                slingLine.SetPosition(0, transform.position + new Vector3(0, 1, 0));
                Vector3 mousePosition = Input.mousePosition;
                Vector3 slingPosition = Camera.main.WorldToScreenPoint(transform.position + new Vector3(0, 1, 0));
                mousePosition.x = (mousePosition.x - slingPosition.x) / 10;
                mousePosition.y = (mousePosition.y - slingPosition.y) / 10;
                Vector3 pos;
                dist = Vector3.Distance(slingLine.GetPosition(0), mousePosition);

                //This loop checks if 
                if (dist > maxLineLenght)
                {
                    pos = slingLine.GetPosition(0) + (mousePosition - slingLine.GetPosition(0)) / (dist / maxLineLenght);
                }
                else
                    pos = mousePosition;
                slingLine.SetPosition(1, pos);
                projectail.transform.position = slingLine.GetPosition(1);
               
            }
            else
            {

                slingLine.enabled = false;
            }
        }
        
        
    }
}
