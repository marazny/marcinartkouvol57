﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Script controls time between sling shots
// Needs improvemnt
public class Restarter : MonoBehaviour {

    private SlingControler restrtedScript;
    private SpriteRenderer cylinder;
    private float time;
    private bool working;
	// Use this for initialization
	void Start () {
        restrtedScript = GetComponent<SlingControler>();
        cylinder = GameObject.Find("basicProjectail").GetComponent<SpriteRenderer>();
	}

    // If bool working is true Update adds time to counter 
    //after it reachche treshhold method stops working and enables sling controling script
	void Update () {
        if (working)
        {
            if (time < 3)
                time += Time.deltaTime;
            else
            {
                cylinder.enabled = false;
                restrtedScript.enabled = true;
                working = false;
            }
        }
	}

    //Method starts counting time and disables Script responsible for controling sling
    public void clock()
    {
        restrtedScript.enabled = false;
        working = true;
        time = 0;
    }

}
